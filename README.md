# Supports des formations SIG & Stat

Ce dépôt permet la mise en ligne des sources (textes, images) des supports de formation SIG & Stat:
![shema](images/shema.png)



-----------



##  Stat 1 : modules socles 0 - 1 - 2

* L'ensemble des supports de formation en ligne se trouvent sur la page dédiée: [https://formationsig.gitlab.io/stat1](https://formationsig.gitlab.io/stat1) 

* Le [diaporama](https://slides.com/archeomatic/stat1)
* Un récapitulatif des [ressources pour l'Analyse de données en aRchéologie](https://formationsig.gitlab.io/stat1/AnalyseDonneesArcheo_Ressources.html)



###	![mod10_clean.png](images/mod10_clean.png) Stat1 - module socle 1.0 - Les données archéologiques : préparer et consolider ses tableaux

* Le support de formation en ligne: [https://formationsig.gitlab.io/stat1/mod_1_0_prepadata](https://formationsig.gitlab.io/stat1/mod_1_0_prepadata/mod_1_0_prepadata.html) 



### ![mod11_stat.png](images/mod11_stat.png) Stat1 - module socle 1.1 - Décrire, analyser et représenter ses données
* Le support de formation en ligne: [https://formationsig.gitlab.io/stat1/mod1_1_stat](https://formationsig.gitlab.io/stat1/mod1_1_stat/mod1_1_stat.html) 



###  ![mod12_R.png](images/mod12_R.png) Stat1 - module socle 1.2 - Initiation au traitement de données sous R
* Le support de formation en ligne: [https://formationsig.gitlab.io/stat1/mod1_2_initR](https://formationsig.gitlab.io/stat1/mod1_2_initR/mod1_2_initR.html) 



##  ![stat21](images/stat21.png) Stat module 2.1 : Comparer plusieurs séries de données

 <u>Attention:</u> les supports de formations sont en cours de rédaction !
* L'ensemble des supports de formation en ligne se trouvent sur la page dédiée: [https://formationsig.gitlab.io/stat21](https://formationsig.gitlab.io/stat2)
* Le [diaporama](https://slides.com/archeomatic/stat2)



## ![sigFT](images/ft_stat.png) Fiches Techniques Stat

Elles sont accessibles en ligne : https://formationsig.gitlab.io/ft_stat

* [Caractériser une variable](https://formationsig.gitlab.io/ft_stat/01_nature_variable.html)

* [mémo variable Qualitative](https://formationsig.gitlab.io/ft_stat/05_memo_variable_QL.html)

* [mémo variable Quantitative](https://formationsig.gitlab.io/ft_stat/06_memo_variable_QT.html)
* [Sémiologie Graphique](https://formationsig.gitlab.io/ft_stat/02_semio_variables_visuelles.html)

* [Analyse bivariée: Arbre de décision](https://formationsig.gitlab.io/ft_stat/03_arbre_de_decision_stat_bivariee.html)

* [Enregistrer un tableau en **csv** avec ![logo_libro24.png](images/logo_libro24.png) LibreOffice Calc](https://formationsig.gitlab.io/ft_stat/04_enregistrer_csv.html)

----------




##  ![sig1](images/sig1.png) SIG 1 : découverte et exploitation des SIG à l'échelle de l'opération

 <u>Attention:</u> les supports de formations sont en cours de rédaction !
* Le support de formation en ligne : [https://formationsig.gitlab.io/sig1](https://formationsig.gitlab.io/sig1)




##  ![sig2](images/sig2.png) SIG 2 : Les figures du rapport avec QGIS

* Le  [support de formation (pdf)](https://drive.google.com/file/d/1rb9WaYOEswQwq0uyhWokVlflBIrir0LQ/view?usp=sharing)  (<u>Attention:</u> ​uniquement accessible aux agents Inrap)
* Le [diaporama](https://slides.com/archeomatic/sig2)



##  ![sig31](images/sig31.png) SIG 3.1 : Bases de Données spatiale et attributaire

* Le support de formation en ligne : [https://formationsig.gitlab.io/sig31](https://formationsig.gitlab.io/sig31)
* Le [diaporama](https://slides.com/archeomatic/sig31)
* La fiche-[mémo SQL](https://formationsig.gitlab.io/sig32/memoSQL/memoSQL.html)



## ![sig32](images/sig32.png) SIG 3.2: Analyses Spatiales

* Le support de formation en ligne : [https://formationsig.gitlab.io/sig32](https://formationsig.gitlab.io/sig32)
* Le [diaporama](https://slides.com/archeomatic/sig32)
* La fiche-[mémo SQL](https://formationsig.gitlab.io/sig32/memoSQL/memoSQL.html)



## ![atelier](images/atelier.png) Ateliers SIG

* [Passer à la version 3.10 de QGIS](https://formationsig.gitlab.io/toqgis3) ![icon](images/sigv324.png) (il existe aussi un [![video](images/video24.png) tutoriel vidéo](https://intranet.inrap.fr/nouveautes-de-la-version-3-qgis-310))
* [Actualisation des connaissances](https://formationsig.gitlab.io/actu) ![icon](images/sigActu24.png)
* [Reprise des données anciennes](https://formationsig.gitlab.io/reprise)  ![icon](images/passif24.png) (il existe aussi un [![video](images/video24.png) tutoriel vidéo](https://intranet.inrap.fr/recuperation-du-passif-produire-des-shp-partir-dun-plan-illustrator-ou-dun-plan-scanne-qgis-310))



##  ![sigFT](images/sigFT2.png) Fiches Techniques SIG

Elles sont accessibles en ligne : https://formationsig.gitlab.io/fiches-techniques

**Nouveautés des versions**

- [Nouveautés de la version 3.12](https://formationsig.gitlab.io/fiches-techniques/nouveautes/nouveautes_3.12.html)
- [Nouveautés de la version 3.14](https://formationsig.gitlab.io/fiches-techniques/nouveautes/nouveautes_3.14.html)

**Prise en main de QGIS**

*   [Installer le logiciel](https://formationsig.gitlab.io/fiches-techniques/priseenmain/00_installer.html)
*   [L'interface de QGIS](https://formationsig.gitlab.io/fiches-techniques/priseenmain/01_interface.html)
*   [Les Systèmes de Coordonnées de Référence](https://formationsig.gitlab.io/fiches-techniques/priseenmain/04_projection_SCR.html)
*   [Constituer un SIG](https://formationsig.gitlab.io/fiches-techniques/priseenmain/02_alimenter.html)
*   [Interrogation simple des données](https://formationsig.gitlab.io/fiches-techniques/priseenmain/03_interroger.html)
*   [Ajouter des extensions](https://formationsig.gitlab.io/fiches-techniques/priseenmain/05_extensions.html)

**Vecteur - Dessin et saisie**

*   [Dessiner dans QGIS : les outils de base](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/01_numerisation_1.html)
*   [Accrochage et topologie](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/02_accrochage.html)
*   [Outil de nœuds](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/03_outil_noeuds.html)
*   [Vérification de validité et de géométrie](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/04_verif_geom.html)
*   [Les courbes de Bézier avec l'extension Bezier Editing](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/06_bezier_editing.html)
*   [Dessiner du pierre à pierre](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/05_pap.html)
*   [Coupes stratigraphiques et QGIS](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/coupe_SIG/coupes_QGIS.html)
*   [Géoréférencer une couche vecteur](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/07_georef_vecteur.html)

**Vecteur - Manipulation et interrogation de données**

*  [Jointure attributaire : liens de 1 à 1](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/02_jointure_att.html)
*  [Relation attributaire : liens de 1 à n](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/09_relation_1_n.html)
*  [Jointure spatiale](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/03_jointure_spatiale.html)
*  [Calculatrice de champs](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/01_calcu_champ.html)
*  [Les opérateurs SQL](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/08_operateurs_sql.html)
*  [Requête attributaire](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/04_requete_att.html)
*  [Requête spatiale](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/05_requete_spatiale.html)
*  [Gestion des identifiants uniques](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/11_gestion_id_uniques.html)
*  [Ajouter une couche de points csv](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/07_couche_points_csv.html)
*  [Analyse par maille ou carte de densité](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/06_analyse_maille.html)  
*  [Calculs d'orientation](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/10_calculs_orientation.html)
*  [Trouver les doublons](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/12_doublons.html)

**Vecteur - Symbologie**

*  [Cartes du BRGM et leurs légendes](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/01_cartes_legendes_BRGM.html)
*  [Ajouter des palettes de couleurs](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/04_palette_couleurs.html)
*  [Connecteurs d'étiquettes (versions antérieures à 3.10)](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/02_connecteur_etiquette.html)
*  [Connecteurs d'étiquettes (version 3.10 et après)](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/03_connecteurs_v3_10.html)
*  [Utiliser les styles de couches et les thèmes de mise en page](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/05_styles_themes.html)
*  [Gérer les superpositions de vestiges](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/06_superpositions.html)

**Raster**

*  [Géoréférencer une image](https://formationsig.gitlab.io/fiches-techniques/raster/02_georef.html) 
*  [Ajouter un flux WMS/WFS](https://formationsig.gitlab.io/fiches-techniques/raster/01_flux_WMS.html)
*  [Réduire des images géoréférencées en lot](https://formationsig.gitlab.io/fiches-techniques/raster/03_reduire_photo_georef.html)
*  [Utiliser les données altimétriques du RGE-alti de l'IGN](https://formationsig.gitlab.io/fiches-techniques/raster/04_rge_alti.html)
*  [Fusionner des dalles MNT en un seul raster](https://formationsig.gitlab.io/fiches-techniques/raster/05_fusion_raster.html)
*   [Gérer la symbologie d'un MNT (Modèle Numérique de Terrain)](https://formationsig.gitlab.io/fiches-techniques/raster/06_symbol_MNT.html)
*  [Générer des courbes de niveau à partir d'un MNT](https://formationsig.gitlab.io/fiches-techniques/raster/07_courbes_niveau.html)
*  [Générer un profil du terrain à partir d'un MNT et calculer le pourcentage de pente](https://formationsig.gitlab.io/fiches-techniques/raster/08_calcul_pente.html)

**Topo**

- L'extension ArchéoCAD [à venir]
- La chaîne opératoire en topographie [à venir]
- Créer un MNT ou Modèle numérique de terrain [à venir]
- Calcul de cubature à partir d'un MNT et utilisation de la calculatrice raster [à venir]

**Mise en page**

* [Créer une carte simple](https://formationsig.gitlab.io/fiches-techniques/miseenpage/01_mise_en_page_base.html)
* [Créer un atlas](https://formationsig.gitlab.io/fiches-techniques/miseenpage/05_atlas_01.html)
* [Mise en valeur des entités de l'atlas](https://formationsig.gitlab.io/fiches-techniques/miseenpage/02_atlas_02.html)
* [Créer un catalogue de sépultures](https://halshs.archives-ouvertes.fr/halshs-03782910) : lien externe vers ![hal24.png](images/hal24.png)HAL
* [Créer et utiliser des modèles de mise en page](https://formationsig.gitlab.io/fiches-techniques/miseenpage/04_modeles.html)
* [Utiliser les projets pré-paramétrés pour générer les cartes de localisation](https://formationsig.gitlab.io/fiches-techniques/miseenpage/03_carte_loc.html)
* [Modifier le nord par défaut dans une mise en page](https://formationsig.gitlab.io/fiches-techniques/miseenpage/06_nord_defaut.html)

**Autres outils**

*  ![qfield](images/qfield24.png) [QField pour QGIS](https://formationsig.gitlab.io/fiches-techniques/autres_outils/01_qfield.html) 


##  ![tuto_video](images/tuto_video.png) Tutoriels vidéos

Des tutoriels vidéos sont disponibles sur l'intranet de l'Inrap (uniquement accessibles aux agents Inrap). Ils sont classés par thèmes:
* [Initiation au SIG: alimenter son projet d'opération avant le décapage](https://intranet.inrap.fr/initiation-au-sig-decouverte-de-qgis-218) (QGIS v 2.18)
  * 1 - Paramétrage du projet, ajout de données vecteur et raster
  * 2 - Ajout de flux WMS et WFS : connexion et récupération des données
  * 3 - Ajout de données tableur et jointure attributaire (lien de 1 à 1)
  * 4 - Alimentation du SIG : géoréférencement raster et numérisation vecteur, gestion de la symbologie
  * 5 - Analyse attributaire : requête SQL
  * 6 - Editer un plan de travail via la capture d'écran géoréférencée
  * 7 - Cartographie et mise en page complète via le composeur
* [Alimenter son projet opération avant décapage : données en ligne et géoréférencement de fonds cartographiques](https://intranet.inrap.fr/alimenter-son-projet-operation-avant-decapage-donnees-en-ligne-open-source-et-georeferencement-de) (QGIS v 2.18)
  * 1 - Fonds de carte de référence : Connexion aux serveurs nationaux et régionaux de flux WMS et WFS et récupération "en dur" des données
  * 2 - Fonds de carte de référence : géoréférencement du cadastre napoléonien puis de l'arrêté de prescription pour numériser l'emprise prescrite
* [Nouveautés de la version 3 (QGIS v 3.10)](https://intranet.inrap.fr/nouveautes-de-la-version-3-qgis-310)
  * 1 - Modifications d'ordre général et le format GeoPackage
  * 2 - Changements dans la fenêtre principale du projet
  * 3 - Modifications pour l'édition de cartes
* [Le SIG au service de la gestion des opérations de terrain](https://intranet.inrap.fr/le-sig-au-service-de-la-gestion-des-operations-de-terrain-qgis-310) (QGIS v 3.10)
  * 1 - Créer son projet (structure du dossier d'opération, paramétrage, SCR), extensions et profils utilisateurs
  * 2 - Alimenter un projet : couches vecteur et raster, gestion des flux WMS et WFS, géoréférencement de raster
  * 3 - Alimenter un projet : numérisation vecteur, vérification de la géométrie et de la topologie
  * 4 - Alimenter un projet : import de données topographiques, vérification de la géométrie et de la topologie
  * 5 - Alimenter un projet : import de données tableur, jointure attributaire pour des liens de 1 à 1, requête attributaire
  * 6 - Alimenter un projet : les jointures spatiales
  * 7 - Analyser son projet : requêtes spatiales, attributaires et statistiques
  * 8 - Afficher les données : catégorisation, cercles proportionnels et création d'une carte de densité
  * 9 - Faire une carte : module de mise en page et exports
* [Récupération du passif : produire des SHP à partir d'un plan Illustrator ou d'un plan scanné](https://intranet.inrap.fr/recuperation-du-passif-produire-des-shp-partir-dun-plan-illustrator-ou-dun-plan-scanne-qgis-310) (QGIS v 3.10)
  * 1 - Ajustement spatial
  * 2 - Vérification de la géométrie et de la topologie
  * 3 - Recours aux scripts pour la création automatique des SHP opération
  * 4 - Jointure spatiale pour récupérer les identifiants
  * 5 - Jointure attributaire (liens de 1 à 1) pour mettre à jour les tables attributaires (typologie et datation)
  * 6 - Géoréférencement de plans anciens (SCR)
* [Systèmes de coordonnées de référence (QGIS v 2.18 et 3.10)](https://intranet.inrap.fr/systemes-de-coordonnees-de-reference-qgis-218-et-310)
  * 1 - Comprendre la composante "géographique" des SIG
  * 2 - Reprojection de couches
* [Composition et mise en page](https://intranet.inrap.fr/composition-et-mise-en-page-qgis-310) (GQIS v 3.10)
  * 1 - Utiliser les styles de couches et les thèmes de carte
  * 2 - Utiliser les projets pré-paramétrés pour générer les cartes de localisation
  * 3 - Créer et utiliser des modèles de mise en page
  * 4 - Créer un atlas
  * 5 - Changer le style de l'entité selon l'atlas
* [Gestion de la cartographie du mobilier et des enregistrements de 1 à n](https://intranet.inrap.fr/gestion-de-la-cartographie-du-mobilier-et-des-enregistrements-de-1-n-qgis-310) (QGIS v 3.10)
  * 1 - Les relations : Création de couches virtuelles pour conserver les liens de 1 à 1
  * 2 - IDSIG ou comment gérer les enregistrements en sondage/fait/us
* [Les règles : affichage ciblé des entités et des étiquettes](https://intranet.inrap.fr/les-regles-affichage-cible-des-entites-et-des-etiquettes-qgis-310) (QGIS v 3.10)
  * 1 - Créer des ensembles de règles pour gérer les notions de stratigraphie (découpage chronologique en état, phasage, période) et gérer l'ordre d'affichage des faits
  * 2 - Créer un étiquetage basé sur des règles pour trier l'affichage de certaines étiquettes
* [Cas d'étude : analyses attributaires et spatiales](https://intranet.inrap.fr/cas-detude-analyses-attributaires-et-spatiales-qgis-310) (QGIS v 3.10)
  * 1 - Calculs d'orientations : faits, tranchées, voies...
  * 2 - Réaliser un dessin de pierre à pierre dans QGIS



-----------



## Ressources et documentation

* [Guide pratique CAVIAR](https://formationsig.gitlab.io/caviar) ![icon](images/sigCaviar24.png)
* [Guide pratique des 6 couches (pdf)](https://docs.google.com/a/inrap.fr/viewer?a=v&pid=sites&srcid=aW5yYXAuZnJ8cmVzZWF1LXJlZmVyZW50cy1zaWd8Z3g6MTY2NTliYzRjNzc1Yjk4Yg) ![icon](images/sig6C24.png)
* Note DST - organisation du NAS [à venir]
* [Raccourcis clavier QGIS (pdf)](https://docs.google.com/a/inrap.fr/viewer?a=v&pid=sites&srcid=aW5yYXAuZnJ8cmVzZWF1LXJlZmVyZW50cy1zaWd8Z3g6OTIyMDEyNTlkMTVlYzBl)