# Supports des formations SIG & Stat


* [Stat 1 : modules socles 0 -1 - 2](/README.md#stat-1--modules-socles-0---1---2)
  * [Stat1 - module socle 1.0 - Les données archéologiques : préparer et consolider ses tableaux](/README.md#stat1---module-socle-10---les-donn&#xE9;es-arch&#xE9;ologiques--pr&#xE9;parer-et-consolider-ses-tableaux)  
  * [Stat1 - module socle 1.1 - Décrire, analyser et représenter ses données](/README.md#stat1---module-socle-11---d&#xE9;crire-analyser-et-repr&#xE9;senter-ses-donn&#xE9;es)
  * [Stat1 - module socle 1.2 - Initiation au traitement de données sous R](/README.md#stat1---module-socle-12---initiation-au-traitement-de-données-sous-r) 
* [Stat module 2.1 : Comparer plusieurs séries de données](/README.md#stat-module-21--comparer-plusieurs-séries-de-données)
* [Fiches Techniques Stat](/README.md#fiches-techniques-stat)
* [SIG 1 : découverte et exploitation des SIG à l'échelle de l'opération](/README.md#sig-1--d&#xE9;couverte-et-exploitation-des-sig-&#xE0;-l&#xE9;chelle-de-lop&#xE9;ration)
* [SIG 2 : Les figures du rapport avec QGIS](/README.md#sig-2--les-figures-du-rapport-avec-qgis)
* [SIG 3.1 : Bases de Données spatiale et attributaire](/README.md#sig-31--bases-de-données-spatiale-et-attributaire)
* [SIG 3.2: Analyses Spatiales](/README.md#sig-32-analyses-spatiales)
* [Ateliers SIG](/README.md#ateliers-sig)
* [Fiches Techniques SIG](/README.md#fiches-techniques-sig)
* [Tutoriels vidéos](/README.md#tutoriels-vidéos)
* [Ressources et documentation](/README.md#ressources-et-documentation)

